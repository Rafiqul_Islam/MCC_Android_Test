package com.rafiq.androidtechnicaltest_mcc;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    Bitmap b;
    ImageView img;
    ImageButton btn_img;
    EditText editName, editAge, editEmail, editPhone;
    Button btnAddData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDb = new DatabaseHelper(this);

        editName = findViewById(R.id.name);
        editAge = findViewById(R.id.age);
        editEmail = findViewById(R.id.email);
        editPhone = findViewById(R.id.phone);

        btn_img = findViewById(R.id.btn_img);

        img = findViewById(R.id.img);

        btn_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //Intent i = new Intent(Intent.ACTION_PICK,
                //MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,22);
            }
        });


        btnAddData = findViewById(R.id.btn_submit);



        AddData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 22 && resultCode == RESULT_OK && data != null)
        {
            btn_img.setVisibility(View.INVISIBLE);
            img.setVisibility(View.VISIBLE);
            b = (Bitmap) data.getExtras().get("data");
            img.setImageBitmap(b);
            //Uri selectedImage = data.getData();
            //img.setImageURI(selectedImage);
        }
    }

    private void AddData() {
        btnAddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            //Log.d("ddd", "hellow : "+editName.getText().toString());

                            boolean isInserted = myDb.insertData(editName.getText().toString().trim(),
                                    editAge.getText().toString().trim(),
                                    editEmail.getText().toString().trim(),
                                    editPhone.getText().toString().trim());
                            if (isInserted == true)
                                Toast.makeText(MainActivity.this, "Submission Successful", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(MainActivity.this, "Invalid", Toast.LENGTH_LONG).show();

                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.galary){
            Intent i = new Intent(MainActivity.this, Galary.class);
            i.putExtra("name",editName.getText().toString());
            i.putExtra("image", b);
            startActivity(i);
        }

        return true;
    }
}
